USE [UserPortal]
GO
/****** Object:  Table [Core].[ApplicationUser]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Core].[ApplicationUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesforceAccountId] [int] NOT NULL,
	[UserId] [nvarchar](256) NOT NULL,
	[IdentityProvider] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](256) NOT NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[FullName] [nvarchar](256) NULL,
	[LastSync] [datetime] NOT NULL,
 CONSTRAINT [PK_Auth0ApplicationUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Core].[EmailResponse]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Core].[EmailResponse](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ToAddress] [varchar](255) NULL,
	[FromAddress] [varchar](255) NULL,
	[ResponseStatusCode] [varchar](max) NULL,
	[ResponseBody] [varchar](max) NULL,
	[ResponseHeaders] [varchar](max) NULL,
	[HtmlContent] [varchar](max) NULL,
	[PlaintextContent] [varchar](max) NULL,
	[Subject] [varchar](max) NULL,
	[SentDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Core].[EmailTemplate]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Core].[EmailTemplate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TemplateId] [int] NOT NULL,
	[TemplateName] [varchar](50) NOT NULL,
	[Description] [varchar](max) NULL,
	[EmailView] [varchar](255) NOT NULL,
	[EmailSubject] [varchar](50) NOT NULL,
	[EmailBody] [varchar](max) NOT NULL,
	[EmailButtonText] [varchar](50) NOT NULL,
	[EmailHeader] [varchar](100) NOT NULL,
	[CanUnsubscribe] [bit] NOT NULL,
	[PlainTextMessage] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Forum].[Post]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Forum].[Post](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TopicId] [int] NOT NULL,
	[ApplicationUserId] [int] NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[Closed] [bit] NOT NULL,
	[ViewCount] [int] NOT NULL,
	[VoteCount] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Forum].[PostAttachment]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Forum].[PostAttachment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PostId] [int] NULL,
	[CommentId] [bigint] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[Path] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Forum].[PostComment]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Forum].[PostComment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationUserId] [int] NOT NULL,
	[PostId] [bigint] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[Body] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Forum].[PostFollow]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Forum].[PostFollow](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PostId] [int] NOT NULL,
	[ApplicationUserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Forum].[PostTopic]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Forum].[PostTopic](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Forum].[PostTopicFollow]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Forum].[PostTopicFollow](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TopicId] [int] NOT NULL,
	[ApplicationUserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[AggregatedCounter]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[AggregatedCounter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [bigint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_CounterAggregated] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Counter]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Counter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [int] NOT NULL,
	[ExpireAt] [datetime] NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_HangFire_Counter] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Hash]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Hash](
	[Key] [nvarchar](100) NOT NULL,
	[Field] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime2](7) NULL,
 CONSTRAINT [PK_HangFire_Hash] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Field] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Job]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Job](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StateId] [bigint] NULL,
	[StateName] [nvarchar](20) NULL,
	[InvocationData] [nvarchar](max) NOT NULL,
	[Arguments] [nvarchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[JobParameter]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobParameter](
	[JobId] [bigint] NOT NULL,
	[Name] [nvarchar](40) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_JobParameter] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[JobQueue]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobQueue](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[Queue] [nvarchar](50) NOT NULL,
	[FetchedAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_JobQueue] PRIMARY KEY CLUSTERED 
(
	[Queue] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[List]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[List](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_List] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Schema]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Schema](
	[Version] [int] NOT NULL,
 CONSTRAINT [PK_HangFire_Schema] PRIMARY KEY CLUSTERED 
(
	[Version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Server]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Server](
	[Id] [nvarchar](200) NOT NULL,
	[Data] [nvarchar](max) NULL,
	[LastHeartbeat] [datetime] NOT NULL,
 CONSTRAINT [PK_HangFire_Server] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Set]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Set](
	[Key] [nvarchar](100) NOT NULL,
	[Score] [float] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Set] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[State]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[State](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Reason] [nvarchar](100) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[Data] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_State] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Jenkins].[BuildHistory]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Jenkins].[BuildHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[JobView] [varchar](255) NOT NULL,
	[JobName] [varchar](255) NOT NULL,
	[JobStatus] [int] NOT NULL,
	[BuildDate] [datetime] NOT NULL,
	[FailedApiCall] [bit] NOT NULL,
	[FailedMessage] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Jenkins].[Folder]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Jenkins].[Folder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Jenkins].[FolderExtension]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Jenkins].[FolderExtension](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FolderId] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [License].[Flex]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[Flex](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NOT NULL,
	[Product] [int] NOT NULL,
	[LicenseType] [bigint] NOT NULL,
	[Format] [nvarchar](50) NULL,
	[Version] [nvarchar](50) NULL,
	[SerialNumber] [nvarchar](50) NULL,
	[HostId] [nvarchar](50) NULL,
	[ServerName] [nvarchar](255) NULL,
	[AssignedUser] [nvarchar](255) NULL,
	[Quantity] [int] NULL,
	[Expiration] [date] NULL,
	[BrickPath] [nvarchar](2555) NULL,
	[IsApproved] [bit] NOT NULL,
	[ApiVersion] [nvarchar](20) NULL,
	[ApiLastSyncDate] [datetime] NULL,
 CONSTRAINT [Pk_LicenseFlex] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [License].[ForgeApplication]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[ForgeApplication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Application] [nvarchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [License].[ForgeHubId]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[ForgeHubId](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdApplication] [int] NOT NULL,
	[HubId] [nvarchar](255) NULL,
	[AccountId] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [License].[ForgeProject]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[ForgeProject](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HubId] [int] NOT NULL,
	[ProjectId] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [License].[NamedUser]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[NamedUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [bigint] NOT NULL,
	[AssignedTo] [bigint] NULL,
	[LicenseName] [nvarchar](20) NOT NULL,
	[Expiration] [date] NOT NULL,
	[SubWarningDays] [int] NOT NULL,
	[Released] [datetime] NULL,
	[AutodeskUsername] [nchar](150) NULL,
	[SubAdmin] [int] NULL,
	[ApiVersion] [nvarchar](20) NULL,
	[ApiLastSyncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [License].[NamedUserChangeTracking]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[NamedUserChangeTracking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LicenseId] [int] NOT NULL,
	[AssignedTo] [bigint] NULL,
	[PreviousAssignedTo] [bigint] NULL,
	[LicenseName] [nvarchar](255) NOT NULL,
	[ModifiedDate] [date] NOT NULL,
	[ChangedByUserId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [License].[NamedUserRefreshToken]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[NamedUserRefreshToken](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[LicenseName] [nvarchar](max) NOT NULL,
	[ComputerId] [nvarchar](max) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[TokenValue] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [License].[NamedUserUsage]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[NamedUserUsage](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[LicenseId] [int] NOT NULL,
	[AssignedTo] [int] NULL,
	[Requested] [datetime] NOT NULL,
	[ProductVersion] [nvarchar](25) NOT NULL,
	[UsernameHash] [nvarchar](255) NOT NULL,
	[UserName] [nvarchar](255) NOT NULL,
	[CompanyName] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [License].[NetworkLicenseActive]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[NetworkLicenseActive](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LicenseId] [uniqueidentifier] NOT NULL,
	[ComputerId] [nvarchar](max) NOT NULL,
	[UserId] [nvarchar](max) NOT NULL,
	[SessionId] [uniqueidentifier] NOT NULL,
	[MachineName] [nvarchar](max) NULL,
	[Date] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [License].[NetworkLicenseAttempts]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[NetworkLicenseAttempts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SerialNumber] [nvarchar](max) NULL,
	[RequestCode] [nvarchar](max) NULL,
	[LicenseName] [nvarchar](max) NULL,
	[ComputerID] [nvarchar](max) NULL,
	[IPAddress] [nvarchar](max) NULL,
	[Date] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [License].[NetworkLicenseData]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[NetworkLicenseData](
	[LicenseId] [uniqueidentifier] ROWGUIDCOL  NULL,
	[LicenseRequestKey] [nvarchar](max) NULL,
	[LicenseTransferKey] [nvarchar](max) NULL,
	[SerialNumber] [nvarchar](max) NULL,
	[ComputerId] [nvarchar](max) NULL,
	[CompanyName] [nvarchar](max) NULL,
	[LegacyCompanyId] [bigint] NULL,
	[IPAddress] [nvarchar](16) NULL,
	[LicenseName] [nvarchar](128) NULL,
	[LicenseStatus] [nvarchar](16) NULL,
	[DateActivated] [datetime] NULL,
	[DateTransferred] [datetime] NULL,
	[DateLastUsed] [datetime] NULL,
	[MaxUsers] [int] NULL,
	[SubscriptionExpiration] [datetime] NULL,
	[License] [nvarchar](max) NULL,
	[RefreshDays] [float] NULL,
	[ProductVersion] [nvarchar](24) NULL,
	[SubWarningDays] [float] NULL,
	[AccountId] [bigint] NULL,
	[ApiVersion] [nvarchar](20) NULL,
	[ApiLastSyncDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [License].[NetworkLicenseFailedChecks]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[NetworkLicenseFailedChecks](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QueryString] [nvarchar](max) NULL,
	[IPAddress] [nvarchar](16) NULL,
	[Date] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [License].[NetworkLicenseInstalls]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[NetworkLicenseInstalls](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LicenseID] [uniqueidentifier] NOT NULL,
	[ComputerID] [nvarchar](max) NOT NULL,
	[InstallDate] [datetime] NULL,
	[License] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [License].[NetworkLicenseUse]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [License].[NetworkLicenseUse](
	[ID] [uniqueidentifier] NOT NULL,
	[LicenseID] [uniqueidentifier] NOT NULL,
	[ComputerID] [nvarchar](max) NOT NULL,
	[UserID] [nvarchar](max) NOT NULL,
	[MachineName] [nvarchar](max) NULL,
	[Acquired] [datetime] NULL,
	[Released] [datetime] NULL,
	[ProductVersion] [nvarchar](24) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Product].[Category]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Product].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[ProductCode] [nvarchar](15) NOT NULL,
	[ShortDescription] [nvarchar](256) NULL,
	[BuiltFor] [nvarchar](256) NULL,
	[LogoRelativePath] [nvarchar](256) NOT NULL,
	[LogoUrl] [nvarchar](256) NULL,
	[DisplayOrder] [int] NOT NULL,
	[Visible] [bit] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Product].[ReleaseNotes]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Product].[ReleaseNotes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[Format] [int] NOT NULL,
	[Content] [nvarchar](max) NULL,
	[LastModified] [date] NOT NULL,
 CONSTRAINT [pk_ProductReleaseNotes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Product].[Version]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Product].[Version](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductCategoryId] [int] NOT NULL,
	[ProductReleaseNotesId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Version] [nvarchar](50) NOT NULL,
	[BuiltForDescription] [nvarchar](255) NULL,
	[BuiltForVersion] [nvarchar](50) NULL,
	[ShortDescription] [nvarchar](255) NULL,
	[LongDescription] [nvarchar](max) NULL,
	[ReleaseDate] [date] NOT NULL,
	[Source] [int] NOT NULL,
	[SourceRelativePath] [nvarchar](max) NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[IsSupportingSoftware] [bit] NOT NULL,
	[IsBeta] [bit] NOT NULL,
	[Region] [int] NOT NULL,
 CONSTRAINT [pk_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Project].[Category]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Project].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[Path] [nvarchar](max) NOT NULL,
	[SearchPattern] [nvarchar](50) NULL,
	[Comments] [nvarchar](max) NULL,
 CONSTRAINT [Pk_ProjectCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Project].[Customer]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Project].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationUserId] [int] NOT NULL,
	[ProjectId] [int] NOT NULL,
 CONSTRAINT [Pk_ProjectCustomer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Project].[Delivery]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Project].[Delivery](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [int] NOT NULL,
	[ProjectMasterId] [int] NOT NULL,
	[SalesforceAccountId] [int] NOT NULL,
	[FolderName] [nvarchar](255) NOT NULL,
	[DeliveryName] [nvarchar](255) NOT NULL,
	[RelativePath] [nvarchar](255) NOT NULL,
	[DeliveryDate] [datetime] NOT NULL,
	[IsApproved] [float] NOT NULL,
	[Description] [nvarchar](255) NULL,
	[UploadedByApplicationUserId] [int] NULL,
	[PhaseId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Project].[InsertErrorTemp]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Project].[InsertErrorTemp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [int] NULL,
	[ProjectMasterId] [int] NULL,
	[SalesforceAccountId] [int] NULL,
	[FolderName] [nvarchar](255) NULL,
	[DeliveryName] [nvarchar](255) NULL,
	[RelativePath] [nvarchar](255) NULL,
	[DeliveryDate] [datetime] NULL,
	[IsApproved] [float] NULL,
	[Description] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Project].[Master]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Project].[Master](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[RootFolder] [nvarchar](255) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_PROJECT_MASTER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Project].[MasterCustomer]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Project].[MasterCustomer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationUserId] [int] NOT NULL,
	[ProjectMasterId] [int] NOT NULL,
	[Comments] [nvarchar](max) NULL,
	[SalesforceAccountId] [int] NULL,
 CONSTRAINT [Pk_ProjectMasterCustomer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Project].[Message]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Project].[Message](
	[SalesforceAccountId] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[ApplicationUserId] [int] NOT NULL,
	[MessageBody] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Project].[Phase]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Project].[Phase](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Project].[Type]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Project].[Type](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Project].[ValidPartner]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Project].[ValidPartner](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PartnerSalesforceAccountId] [int] NOT NULL,
	[CustomerSalesforceAccountId] [int] NOT NULL,
	[TypeId] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [Salesforce].[Account]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Salesforce].[Account](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SalesforceIdentifier] [nvarchar](255) NOT NULL,
	[SalesforceName] [nvarchar](255) NOT NULL,
	[SalesforceShortName] [nvarchar](255) NOT NULL,
	[SalesforceDomainNames] [nvarchar](255) NOT NULL,
	[SalesforceAddress] [nvarchar](255) NULL,
	[SalesforceAccountOwnerEmailAddress] [nvarchar](255) NULL,
	[Region] [int] NOT NULL,
	[ApiVersion] [nvarchar](20) NULL,
	[ApiLastSyncDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Salesforce].[EmailDomain]    Script Date: 11/14/2024 10:12:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Salesforce].[EmailDomain](
	[SalesforceAccountId] [int] NOT NULL,
	[Domain] [nvarchar](255) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [License].[NetworkLicenseActive] ADD  CONSTRAINT [DF_NetworkLicenseActive_Date]  DEFAULT (getutcdate()) FOR [Date]
GO
ALTER TABLE [License].[NetworkLicenseAttempts] ADD  CONSTRAINT [DF_AuthLicenseAttempts_Date]  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [License].[NetworkLicenseData] ADD  CONSTRAINT [DF_SDLicenseData_LicenseID]  DEFAULT (newid()) FOR [LicenseId]
GO
ALTER TABLE [License].[NetworkLicenseData] ADD  CONSTRAINT [DF_SDLicenseData_UseCount]  DEFAULT ((0)) FOR [MaxUsers]
GO
ALTER TABLE [License].[NetworkLicenseFailedChecks] ADD  CONSTRAINT [DF_FailedLicenseChecks_Date]  DEFAULT (getdate()) FOR [Date]
GO
ALTER TABLE [License].[NetworkLicenseInstalls] ADD  CONSTRAINT [DF_NetworkInstalls_InstallDate]  DEFAULT (getdate()) FOR [InstallDate]
GO
ALTER TABLE [License].[NetworkLicenseUse] ADD  CONSTRAINT [DF_NetworkLicenseUse_DateActivated]  DEFAULT (getutcdate()) FOR [Acquired]
GO
ALTER TABLE [Product].[ReleaseNotes] ADD  DEFAULT ((0)) FOR [IsDefault]
GO
ALTER TABLE [Product].[ReleaseNotes] ADD  DEFAULT (getdate()) FOR [LastModified]
GO
ALTER TABLE [Product].[Version] ADD  DEFAULT ((1)) FOR [ProductReleaseNotesId]
GO
ALTER TABLE [Product].[Version] ADD  DEFAULT (getdate()) FOR [ReleaseDate]
GO
ALTER TABLE [Product].[Version] ADD  DEFAULT ((1)) FOR [Source]
GO
ALTER TABLE [Product].[Version] ADD  DEFAULT ((0)) FOR [IsVisible]
GO
ALTER TABLE [Product].[Version] ADD  DEFAULT ((0)) FOR [IsSupportingSoftware]
GO
ALTER TABLE [Product].[Version] ADD  DEFAULT ((0)) FOR [IsBeta]
GO
ALTER TABLE [Product].[Version] ADD  DEFAULT ((1)) FOR [Region]
GO
ALTER TABLE [Project].[Delivery] ADD  CONSTRAINT [DF_Default_Project_Phase]  DEFAULT ((1)) FOR [PhaseId]
GO
ALTER TABLE [Project].[Master] ADD  CONSTRAINT [DF_PROJECT_MASTER_IS_ACTIVE]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [Salesforce].[Account] ADD  DEFAULT ((1)) FOR [Region]
GO
ALTER TABLE [HangFire].[JobParameter]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_JobParameter_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[JobParameter] CHECK CONSTRAINT [FK_HangFire_JobParameter_Job]
GO
ALTER TABLE [HangFire].[State]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_State_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[State] CHECK CONSTRAINT [FK_HangFire_State_Job]
GO
ALTER TABLE [Product].[Version]  WITH CHECK ADD  CONSTRAINT [fk_Product_ProductCategoryId] FOREIGN KEY([ProductCategoryId])
REFERENCES [Product].[Category] ([Id])
GO
ALTER TABLE [Product].[Version] CHECK CONSTRAINT [fk_Product_ProductCategoryId]
GO
ALTER TABLE [Product].[Version]  WITH CHECK ADD  CONSTRAINT [fk_Product_ProductReleaseNotesId] FOREIGN KEY([ProductReleaseNotesId])
REFERENCES [Product].[ReleaseNotes] ([Id])
GO
ALTER TABLE [Product].[Version] CHECK CONSTRAINT [fk_Product_ProductReleaseNotesId]
GO
ALTER TABLE [Project].[Customer]  WITH CHECK ADD FOREIGN KEY([ProjectId])
REFERENCES [Project].[Delivery] ([Id])
GO
ALTER TABLE [Project].[Delivery]  WITH CHECK ADD FOREIGN KEY([ProjectMasterId])
REFERENCES [Project].[Master] ([Id])
GO
ALTER TABLE [Project].[Delivery]  WITH CHECK ADD FOREIGN KEY([SalesforceAccountId])
REFERENCES [Salesforce].[Account] ([Id])
GO
ALTER TABLE [Project].[Delivery]  WITH CHECK ADD FOREIGN KEY([TypeId])
REFERENCES [Project].[Type] ([Id])
GO
ALTER TABLE [Project].[MasterCustomer]  WITH CHECK ADD FOREIGN KEY([ProjectMasterId])
REFERENCES [Project].[Master] ([Id])
GO
ALTER TABLE [Project].[Message]  WITH CHECK ADD FOREIGN KEY([ApplicationUserId])
REFERENCES [Core].[ApplicationUser] ([Id])
GO
ALTER TABLE [Project].[Message]  WITH CHECK ADD FOREIGN KEY([SalesforceAccountId])
REFERENCES [Salesforce].[Account] ([Id])
GO
ALTER TABLE [Project].[Message]  WITH CHECK ADD FOREIGN KEY([TypeId])
REFERENCES [Project].[Type] ([Id])
GO
ALTER TABLE [Project].[ValidPartner]  WITH CHECK ADD FOREIGN KEY([CustomerSalesforceAccountId])
REFERENCES [Salesforce].[Account] ([Id])
GO
ALTER TABLE [Project].[ValidPartner]  WITH CHECK ADD FOREIGN KEY([PartnerSalesforceAccountId])
REFERENCES [Salesforce].[Account] ([Id])
GO
ALTER TABLE [Salesforce].[EmailDomain]  WITH CHECK ADD  CONSTRAINT [fk_SalesforceEmailDomains_SalesforceAccount_Id] FOREIGN KEY([SalesforceAccountId])
REFERENCES [Salesforce].[Account] ([Id])
GO
ALTER TABLE [Salesforce].[EmailDomain] CHECK CONSTRAINT [fk_SalesforceEmailDomains_SalesforceAccount_Id]
GO

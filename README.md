# 🔞 Step by step

## ⚠️ Nếu gặp lỗi khi clone / build image, run cmd as administrator & chậy lệnh 
- ```
  git config --system core.longpaths true
  ```
- ```Clone lại project```

## ⭐ Cấu hình kết nối đến MSSQL & PostgreSQL tại Dockerfile
- ```ENV SRC_*```: Cấu hình kết nối đến MSSQL
- ```ENV DST_*```: Cấu hình kết nối đến PostgreSQL
- ***Tạo database tại PostgreSQL đúng với tên đã chỉ định trong Dockerfile (CASE-SENSITIVE)***
- ```ENV SCHEMA_FILE```: tên của script được tạo ra tự động từ MSSQL

## ⭐ Mở ```SQL Server Management Studio```
- Click chuột phải vào database mong muốn
- Chọn Task -> Genarate Scripts... 

  ![alt text](./images/gen_script.png)
- Chọn ```Next```
- Chọn ```Select specific database objects```, click chọn ```Tables```

  ![alt text](./images/step_02.png)
- Chọn ```Save as script file```, đặt tên file và lưu vào thư mục ```conf``` của thư mục hiện tại, ***cập nhật tên file vào ENV SCHEMA_FILE trong Dockerfile*** 

  ![alt text](./images/step_03.png)
- Chọn ```Next```
- Chọn ```Next```
- ```Finish```

## ⭐ Mở cmd tại thư mục hiện tại và chậy lần lượt các lệnh sau

- ``` 
   docker build -t sqlserver2psql . 
   ```

- ```
   docker run -it --name sqlserver2psql --rm --mount type=bind,source=${PWD}/conf,target=/opt/data_migration/conf sqlserver2psql
   ```
- ***Click "Share it" để cho phép share data giữa host & container (if asked)***

   ![alt text](./images/share_it.png)

#### Step 01
- ```
    bash /scripts/step_01.sh
   ```
    
- ***Thực hiện các bước sau trước khi run step_02.sh (vì cần viết hẳn tên schema ra)***
    - ```
      vi before.sql
      ```
    - Nhấn ```i``` để vào insert mode
    - Sửa ```CREATE SCHEMA IF NOT EXISTS "";``` => ```CREATE SCHEMA IF NOT EXISTS public;```
    - Nhấn ```Esc``` để thoát insert mode
    - Nhập ```:wq``` để lưu và thoát

- #### Trường hợp table có dữ liệu lớn quá, cần tách nhỏ các table để chậy migrate riêng (performance) #####
    - ##### Copy file migration.kjb từ container vào host để tiện edit #####
        - Mở cmd tại thư mục ở host nơi mà muốn copy file từ container vào & chậy lệnh
        - ```
          docker cp sqlserver2psql:/opt/data_migration/kettlejobs/migration.kjb .
          ```
    - ##### Mở file ```migration.kjb```
        - Mỗi tag ```<entry>``` đại diện cho 1 table
        - Mỗi tag ```<hop>``` chỉ dẫn thứ tự thực thi table cho Kettle Job
            - tag ```<from>``` chỉ định table chạy trước
            - tag ```<to>``` chỉ định table chạy sau
        - Thay đổi thứ tự thực thi table để bỏ những table có dữ liệu lớn, cần giữ lại START, SQL SCRIPT START, SQL SCRIPT END
    - ##### Quay lại cmd & copy ngược lại từ host vào container
        - ```
          docker cp ./migration.kjb sqlserver2psql:/opt/data_migration/kettlejobs/migration.kjb
          ```
- #### Trường hợp không muón migrate dữ liệu của một vài trường trong table #####
    - ##### Copy file public-{{tableName}}.ktr từ container vào host để tiện edit #####
        - Mở cmd tại thư mục ở host nơi mà muốn copy file từ container vào & chậy lệnh
        - ```
          docker cp sqlserver2psql:/opt/data_migration/kettlejobs/public-{{tableName}}.ktr .
          ```
    - ##### Mở file ```public-{{tableName}}.ktr```
        - Tìm dòng ```<sql>SELECT``` bỏ đi trường dữ liệu không muốn migrate
    - ##### Quay lại cmd & copy ngược lại từ host vào container  
        - ```
          docker cp ./public-{{tableName}}.ktr sqlserver2psql:/opt/data_migration/kettlejobs/public-{{tableName}}.ktr
          ```
#### Step 02
- ``` 
   bash /scripts/step_02.sh 
   ```
##### Thoát
- ```
  exit
  ```
- Mở PostgreSQL kiểm tra ✔️
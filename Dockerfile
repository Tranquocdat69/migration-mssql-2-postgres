FROM adoptopenjdk/openjdk8

RUN apt-get update; apt-get install perl netcat -y; \
    apt-get install wget unzip postgresql-client -y
RUN apt-get update; apt-get install vim -y
RUN apt-get update; apt-get install iputils-ping -y

ENV SRC_HOST=192.168.81.119
ENV SRC_PORT=1433
ENV SRC_USER=sa
ENV SRC_PWD=123qwe
ENV SRC_DB=UserPortal

ENV DST_HOST=192.168.81.119
ENV DST_PORT=5432
ENV DST_USER=postgres
ENV DST_PWD=123qwe
ENV DST_DB=UserPortal

ENV SCHEMA_FILE=schema.sql

ENV MIGRATIONDIR=/opt/data_migration

COPY ./pdi-ce-9.3.0.0-428/data-integration $MIGRATIONDIR/data-integration
RUN chmod -R +x $MIGRATIONDIR/data-integration/*.sh

COPY ./jtds-1.3.1-dist/jtds-*.jar $MIGRATIONDIR/data-integration/lib/
COPY ./mssql-jdbc-12.2.0.jre11.jar $MIGRATIONDIR/data-integration/lib/
COPY ./sqlserver2pgsql.pl $MIGRATIONDIR

RUN sed -i 's#<attribute><code>EXTRA_OPTION_MSSQL.instance#<attribute><code>EXTRA_OPTION_MSSQL.instance#g' $MIGRATIONDIR/sqlserver2pgsql.pl; \
    sed -i "s#<attribute><code>EXTRA_OPTION_POSTGRESQL.reWriteBatchedInserts#<attribute><code>EXTRA_OPTION_POSTGRESQL.reWriteBatchedInserts#g" $MIGRATIONDIR/sqlserver2pgsql.pl; \
    chmod +x $MIGRATIONDIR/sqlserver2pgsql.pl

COPY ./scripts /scripts

RUN chmod +x /scripts/*.sh

RUN mkdir -p $MIGRATIONDIR

WORKDIR /opt/data_migration